package procempa.com.dto;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe principal para a geracao da inscricao comercial, nela esta associada o estabelecimento que sera cadastrado/atualizado
 * @author helison
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InscricaoComercial {
	
	/**
	 * O numero da inscricao a ser retornado pelo SIAT
	 */
	private  Long numInscricao;
	/**
	 * O horario de criacao do registro
	 */
	private  Calendar version;
	/**
	 * O estabelecimento que sera gerada a inscricao municipal
	 */
	private PessoaJuridica estabelecimento;

	/**
	 * Cria o objeto InscricaoComercial definindo o version para a hora de criacao do objeto
	 */	
	public InscricaoComercial() {
		this.setVersion(Calendar.getInstance());
	}
	
	/**
	 * o numero da inscricao a ser retornado pelo servico rest do SIAT-API
	 * @return the numInscricao
	 */
	public Long getNumInscricao() {
		return numInscricao;
	}
	/**
	 * @param numInscricao the numInscricao to set
	 */
	public void setNumInscricao(Long numInscricao) {
		this.numInscricao = numInscricao;
	}
	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}
	/**
	 * @return the estabelecimento
	 */
	public PessoaJuridica getEstabelecimento() {
		return estabelecimento;
	}
	/**
	 * Define o Estabelecimento que será enviado para criação da inscrição
	 * @param estabelecimento the estabelecimento to set
	 */
	public void setEstabelecimento(PessoaJuridica estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	
}