package procempa.com.dto;

import java.util.Calendar;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe utilizada para criacao de pessoas fisicas
 * Uma pessoa fisica pode ser um socio de uma empresa ou um contador
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaFisica extends Pessoa {

	/**
	 * O numero do RG da pessoa fisica
	 */
	private String numRg;
	/**
	 * A data de nascimento da pessoa
	 */
	private Calendar dtaNascimento;
	/**
	 * O sexo da pessoa
	 */
	private String sexo;
	
	/**
	 * Identifica se a pessoa eh estrangeira, por padrão "N"
	 */
	private String indEstrangeiro = "N";
	
	public PessoaFisica() {
	}
	/**
	 * Construtor da pessoa fisica
	 * @param numDocumento o CPF da pessoa fisica
	 * @param desNome o nome da pessoa fisica
	 * @param desNomeReduzido o nome reduzido da pessoa fisica caso ele tenha mais de 100 caracteres
	 * @param dtaCadastro a data em que a pessoa fisica foi cadastrada 
	 * @param codComplementoNome o codigo de complemento do nome da pessoa juridica
	 * @param enderecos a lista contendo os enderecos da pessoa fisica
	 * @param contados a lista contendo os contatos da pessoa fisica
	 */
	public PessoaFisica(String numDocumento, String desNome, String desNomeReduzido, Calendar dtaCadastro,
			int codComplementoNome, List<Endereco> enderecos, List<Contato> contados) {
		super(numDocumento, desNome, desNomeReduzido, dtaCadastro, codComplementoNome,  enderecos, contados);
	}
	
	/**
	 * Construtor utilizado para transformar a Pesssoa em PessoaFisica
	 * @param pessoa a pessoa para definir como PessoaFisica
	 */
	public PessoaFisica (Pessoa pessoa){
		super(pessoa.getNumDocumento(),
				pessoa.getDesNome(),
				pessoa.getDesNomeReduzido(),
				pessoa.getDtaCadastro(),
				pessoa.getCodComplementoNome(),
				pessoa.getEnderecos(),
				pessoa.getContatos());
	}
	
	/**
	 * @return the numRg
	 */
	public String getNumRg() {
		return numRg;
	}
	/**
	 * @param numRg the numRg to set
	 */
	public void setNumRg(String numRg) {
		this.numRg = numRg;
	}
	/**
	 * @return the dtaNascimento
	 */
	public Calendar getDtaNascimento() {
		return dtaNascimento;
	}
	/**
	 * @param dtaNascimento the dtaNascimento to set
	 */
	public void setDtaNascimento(Calendar dtaNascimento) {
		this.dtaNascimento = dtaNascimento;
	}
	/**
	 * @return the sexo
	 */
	public String getSexo() {
		return sexo;
	}
	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getIndEstrangeiro() {
		return indEstrangeiro;
	}
	public void setIndEstrangeiro(String indEstrangeiro) {
		this.indEstrangeiro = indEstrangeiro;
	}

}
