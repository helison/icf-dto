package procempa.com.dto;

import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe utilizada para a criacao de pessoa juridica Uma pessoa juridica se
 * encaixa nos seguintes papeis: Socio, Contador, empresa principal, empresa
 * matriz de Porto Alegre
 * 
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PessoaJuridica extends Pessoa {

	public interface TipoEmpresa {
		int INDIVIDUAL = 304;
		int SOCIEDADE = 303;
		int FILIAL_DE_FI_COM_SEDE_FORA = 305;
		int CONSORCIO = 11985;
		int GRUPO_DE_SOCIEDADE = 11986;
		int FILIAL_DE_CONSORC_GRUPO_SOC_COM_SEDE_FORA = 11987;
		int FILIAL_DE_CONSORC_GRUPO_SOC_EM_OUTRA_UF = 11988;
		int FILIAL_DE_CONSORC_NA_MESMA_UF_DA_SEDE = 11989;
		int PNE_EMPRESARIO = 11990;
		int FILIAL_DE_FI_EM_OUTRA_UF = 11980;
		int FILIAL_DE_FI_NA_MESMA_UF_DA_SEDE = 11981;
		int FILIAL_DE_SOC_COM_SEDE_FORA = 11982;
		int FILIAL_DE_SOC_EM_OUTRA_UF = 11983;
		int FILIAL_DE_SOC_NA_MESMA_UF_DA_SEDE = 11984;
	}

	public interface RegimeTributacao {
		int REGIME_GERAL = 1;
		int SIMPLES_NACIONAL = 2;
		int NAO_SE_APLICA = 3;
	}

	public interface FormaTributacao {
		int RECEITA_BRUTA = 1;
		int ME_EPP = 2;
		int MEI = 3;
	}

	public interface TipoOrgaoRegulamentador {
		int JUNTA_COMERCIAL = 300;
		int CARTORIO_DE_REGISTRO_DE_PJ = 299;
		int OAB = 301;
		int LEI_DECRETO = 302;
	}
	
	public interface TipoEvento {
		int INCLUSAO = 1;
		int ALTERACAO = 2;
		int BAIXA = 3;
	}

	/**
	 * Empresa matriz
	 */
	@Valid
	private PessoaJuridica matriz;
	/**
	 * O numero do orgao regulamentador
	 */
	private String norOrgaoRegulamentador;
	/**
	 * O tipo de orgao regulamentador
	 */

	private Integer tipoOrgaoRegulamentador;
	/**
	 * o Tipo de empresa (individual, sociedade, filial ...)
	 */
	private Integer intTipoEmpresa;
	/**
	 * Regime de Tributacao (Simples Nacional, Regime Geral)
	 */
	@Min(value = 1, message = "Devera informado o Regime de Tributacao, sendo regime geral 1, simples nacional 2 ou nao se aplica 3")
	@Max(value = 3, message = "Devera informado o Regime de Tributacao, sendo regime geral 1, simples nacional 2 ou nao se aplica 3")
	private Integer regimeTributacao;
	/**
	 * Forma da tributacao (ME/EPP, MEI, Receita Bruta, Não se aplica)
	 */
	@Min(value = 1, message = "Devera ser informado se a empresa se encaixa como RB 1, ME/EPP 2 ou MEI 3")
	@Max(value = 3, message = "Devera ser informado se a empresa se encaixa como RB 1, ME/EPP 2 ou MEI 3")
	private Integer formaTributacao;
	/**
	 * Data de registro no orgao responsavel
	 */
	private Calendar dtaRegistroOrgao;
	/**
	 * Porte da Empresa
	 */
	private String desRegPorteEmpresa;
	/**
	 * Nome Fantasia da empresa
	 */
	private String nmeFantasia;
	/**
	 * A data do cancelamento da empresa, caso tenha sido cancelada
	 */
	private Calendar dtaCancelamento;

	/**
	 * A data de baixa da empresa
	 */
	private Calendar dtaBaixa;

	/**
	 * tipo de evento na empresa 1-Inclusao, 2-Alteracao, 3-Baixa da Empresa
	 */
	private Integer tipoEvento;

	/**
	 * A data do cancelamento da empersa, caso tenha sido cancelada
	 */
	@NotNull(message = "A data de inicio das atividades da empresa deve ser informada")
	private Calendar dtaInicioAtividades;

	/**
	 * A lista contendo o contador responsável e a empresa de contabilidade
	 * responsável(se houver)
	 */
	private List<Contador> contadoresResponsaveis;
	/**
	 * A lista contendo todos os sócios da empresa
	 */
	@Valid
	@NotEmpty
	@Size(min = 1, message = "A empresa devera possuir ao menos um socio")
	private List<Socio> socios;
	/**
	 * Os dados da Receita Federal
	 */
	private DadosRFB dadosRFB;
	/**
	 * Caso a empresa esteja cadastrada no Simples Nacional ou já tenha sido
	 * cadastrada (ME/EPP ou MEI), os períodos desse cadastro
	 */
	private List<PeriodoSimplesNacional> periodos;
	/**
	 * A lista contendo todas as atividades com os CNAES da empresa
	 */
	@Valid
	private List<Atividade> atividades;

	/**
	 * O objeto social da atividade da empresa
	 */
	@NotEmpty(message = "O objeto social da empresa devera ser informado")
	private String desObjetoSocial;
	
	
	private List<Evento> eventos;

	public PessoaJuridica() {
	}

	/**
	 * Construtor da pessoa juridica
	 * 
	 * @param numDocumento       o CNPJ da empresa
	 * @param desNome            o nome da empresa
	 * @param desNomeReduzido    o nome reduzido da empresa caso ele tenha mais de
	 *                           100 caracteres
	 * @param dtaCadastro        a data em que a empresa foi cadastrada
	 * @param codComplementoNome o codigo de complemento do nome da pessoa juridica
	 * @param enderecos          a lista contendo os enderecos da empresa
	 * @param contados           a lista contendo os contatos da empresa
	 */
	public PessoaJuridica(String numDocumento, String desNome, String desNomeReduzido, Calendar dtaCadastro,
			Integer codComplementoNome, List<Endereco> enderecos, List<Contato> contados) {
		super(numDocumento, desNome, desNomeReduzido, dtaCadastro, codComplementoNome, enderecos, contados);
	}

	/**
	 * Construtor utilizado para transformar a Pesssoa em PessoaJuridica
	 * 
	 * @param pessoa a Pessoa que sera utilizada como base para criar a
	 *               PesoaJuridica
	 */
	public PessoaJuridica(Pessoa pessoa) {
		super(pessoa.getNumDocumento(), pessoa.getDesNome(), pessoa.getDesNomeReduzido(), pessoa.getDtaCadastro(),
				pessoa.getCodComplementoNome(), pessoa.getEnderecos(), pessoa.getContatos());
	}

	/**
	 * @return the norOrgaoRegulamentador
	 */
	public String getNorOrgaoRegulamentador() {
		return norOrgaoRegulamentador;
	}

	/**
	 * @param norOrgaoRegulamentador the norOrgaoRegulamentador to set
	 */
	public void setNorOrgaoRegulamentador(String norOrgaoRegulamentador) {
		this.norOrgaoRegulamentador = norOrgaoRegulamentador;
	}

	/**
	 * @return the tipoOrgaoRegulamentador
	 */
	public Integer getTipoOrgaoRegulamentador() {
		return tipoOrgaoRegulamentador;
	}

	/**
	 * @param tipoOrgaoRegulamentador the tipoOrgaoRegulamentador to set
	 */
	public void setTipoOrgaoRegulamentador(Integer tipoOrgaoRegulamentador) {
		this.tipoOrgaoRegulamentador = tipoOrgaoRegulamentador;
	}

	/**
	 * @return the intTipoEmpresa
	 */
	public Integer getIntTipoEmpresa() {
		return intTipoEmpresa;
	}

	/**
	 *
	 * <table border = 1>
	 * <caption>Tipos de Empresa</caption> <tbody>
	 * <tr>
	 * <td ><strong>Codigo</strong></td>
	 * <td ><strong>Tipo de Empresa</strong></td>
	 * </tr>
	 * <tr>
	 * <td>304</td>
	 * <td>Individual</td>
	 * </tr>
	 * <tr>
	 * <td>303</td>
	 * <td>Sociedade</td>
	 * </tr>
	 * <tr>
	 * <td>305</td>
	 * <td>Filial de FI com sede fora</td>
	 * </tr>
	 * <tr>
	 * <td>11985</td>
	 * <td>Consorcio</td>
	 * </tr>
	 * <tr>
	 * <td>11986</td>
	 * <td>Grupo de Sociedade</td>
	 * </tr>
	 * <tr>
	 * <td>11987</td>
	 * <td>Filial de Consorcio/Grupo Sociedade com sede fora</td>
	 * </tr>
	 * <tr>
	 * <td>11988</td>
	 * <td>Filial de Consorcio/Grupo Sociedade em outra UF</td>
	 * </tr>
	 * <tr>
	 * <td>11989</td>
	 * <td>Filial de consorcio na mesma UF da Sede</td>
	 * </tr>
	 * <tr>
	 * <td>11990</td>
	 * <td>PNE empresario</td>
	 * </tr>
	 * <tr>
	 * <td>11980</td>
	 * <td>Filial de FI em outra UF</td>
	 * </tr>
	 * <tr>
	 * <td>11981</td>
	 * <td>Filial de FI na mesma UF da Sede</td>
	 * </tr>
	 * <tr>
	 * <td>11982</td>
	 * <td>Filial de Soc. com sede fora</td>
	 * </tr>
	 * <tr>
	 * <td>11983</td>
	 * <td>Filial de Soc. em outra UF</td>
	 * </tr>
	 * <tr>
	 * <td>11984</td>
	 * <td>Filial de Soc. na mesma UF da Sede</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * 
	 * @param intTipoEmpresa the intTipoEmpresa to set
	 */
	public void setIntTipoEmpresa(Integer intTipoEmpresa) {
		this.intTipoEmpresa = intTipoEmpresa;
	}

	/**
	 * @return the dtaRegistroOrgao
	 */
	public Calendar getDtaRegistroOrgao() {
		return dtaRegistroOrgao;
	}

	/**
	 * @param dtaRegistroOrgao the dtaRegistroOrgao to set
	 */
	public void setDtaRegistroOrgao(Calendar dtaRegistroOrgao) {
		this.dtaRegistroOrgao = dtaRegistroOrgao;
	}

	/**
	 * @return the desRegPorteEmpresa
	 */
	public String getDesRegPorteEmpresa() {
		return desRegPorteEmpresa;
	}

	/**
	 * @param desRegPorteEmpresa the desRegPorteEmpresa to set
	 */
	public void setDesRegPorteEmpresa(String desRegPorteEmpresa) {
		this.desRegPorteEmpresa = desRegPorteEmpresa;
	}

	/**
	 * @return the nmeFantasia
	 */
	public String getNmeFantasia() {
		return nmeFantasia;
	}

	/**
	 * @param nmeFantasia the nmeFantasia to set
	 */
	public void setNmeFantasia(String nmeFantasia) {
		this.nmeFantasia = nmeFantasia;
	}

	/**
	 * @return the dtaCancelamento
	 */
	public Calendar getDtaCancelamento() {
		return dtaCancelamento;
	}

	/**
	 * @param dtaCancelamento the dtaCancelamento to set
	 */
	public void setDtaCancelamento(Calendar dtaCancelamento) {
		this.dtaCancelamento = dtaCancelamento;
	}

	/**
	 * @return the contadoresResponsaveis
	 */
	public List<Contador> getContadoresResponsaveis() {
		return contadoresResponsaveis;
	}

	/**
	 * @param contadoresResponsaveis the contadorResponsavel to set
	 */
	public void setContadoresResponsaveis(List<Contador> contadoresResponsaveis) {
		this.contadoresResponsaveis = contadoresResponsaveis;
	}

	/**
	 * @return the socios
	 */
	public List<Socio> getSocios() {
		return socios;
	}

	/**
	 * @param socios the socios to set
	 */
	public void setSocios(List<Socio> socios) {
		this.socios = socios;
	}

	/**
	 * @return the dadosRFB
	 */
	public DadosRFB getDadosRFB() {
		return dadosRFB;
	}

	/**
	 * @param dadosRFB the dadosRFB to set
	 */
	public void setDadosRFB(DadosRFB dadosRFB) {
		this.dadosRFB = dadosRFB;
	}

	/**
	 * @return the peridos
	 */
	public List<PeriodoSimplesNacional> getPeriodos() {
		return periodos;
	}

	/**
	 * @param peridos the peridos to set
	 */
	public void setPeriodos(List<PeriodoSimplesNacional> peridos) {
		this.periodos = peridos;
	}

	/**
	 * @return the atividades
	 */
	public List<Atividade> getAtividades() {
		return atividades;
	}

	/**
	 * @param atividades the atividades to set
	 */
	public void setAtividades(List<Atividade> atividades) {
		this.atividades = atividades;
	}

	/**
	 * @return the regimeTributacao
	 */
	public Integer getRegimeTributacao() {
		return regimeTributacao;
	}

	/**
	 * Define o Regime de tributacao, pode receber os seguintes valores:
	 * 
	 * <pre>
	 * 1 - Regime Geral LCM 7/73
	 * 2 - Simples Nacional LC 123/06
	 * 3 - Nao se aplica
	 * </pre>
	 * 
	 * @param regimeTributacao the regimeTributacao to set
	 */
	public void setRegimeTributacao(Integer regimeTributacao) {
		this.regimeTributacao = regimeTributacao;
	}

	/**
	 * @return the formaTributacao
	 */
	public Integer getFormaTributacao() {
		return formaTributacao;

	}

	/**
	 * Define a forma de tributacao, informar os seguintes valores:
	 * 
	 * <pre>
	 * 1 - Receita Bruta - Em caso de regime de tributaÃ§Ã£o geral
	 * 2 - ME / EPP - Em caso de simples Nacional
	 * 3 - MEI - Para MEI enquadrado no simples nacional
	 * 4 - Outro
	 * </pre>
	 * 
	 * @param formaTributacao the formaTributacao to set
	 */
	public void setFormaTributacao(Integer formaTributacao) {
		this.formaTributacao = formaTributacao;
	}

	/**
	 * 
	 * @return a matriz da filial
	 */
	public PessoaJuridica getMatriz() {
		return matriz;
	}

	/**
	 * 
	 * @param matriz define matriz da filial
	 */
	public void setMatriz(PessoaJuridica matriz) {
		this.matriz = matriz;
	}

	/**
	 * 
	 * @return o objeto social da empresa
	 */
	public String getDesObjetoSocial() {
		return desObjetoSocial;
	}

	/**
	 * @param desObjetoSocial define o objeto social da empresa
	 */
	public void setDesObjetoSocial(String desObjetoSocial) {
		this.desObjetoSocial = desObjetoSocial;
	}

	/**
	 * 
	 * @return a data de inicio das atividades
	 */
	public Calendar getDtaInicioAtividades() {
		return dtaInicioAtividades;
	}

	/**
	 * 
	 * @param dtaInicioAtividades define a data de inicio das atividades
	 */
	public void setDtaInicioAtividades(Calendar dtaInicioAtividades) {
		this.dtaInicioAtividades = dtaInicioAtividades;
	}

	/**
	 * 
	 * @return a data de baixa da empresa
	 */
	public Calendar getDtaBaixa() {
		return dtaBaixa;
	}

	/**
	 * 
	 * @param dtaBaixa a data de baixa da empresa
	 */

	public void setDtaBaixa(Calendar dtaBaixa) {
		this.dtaBaixa = dtaBaixa;
	}

	/**
	 * 
	 * @return o tipo do evento
	 */
	public Integer getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * O tipo de evento a ser considerado Utilizado principalmente para indicacao de
	 * baixa
	 * 
	 * @param tipoEvento 1-Inclusao, 2-Alteracao, 3-Baixa da Empresa
	 */
	public void setTipoEvento(Integer tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}
}
