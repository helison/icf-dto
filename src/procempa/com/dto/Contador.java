package procempa.com.dto;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * O contador responsavel pela empresa contabilidade da empresa.
 * Instanciar somente o contador ou a empresa de contabilidade, dependendo do que esta sendo definido
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contador {
	/**
	 * o numero de inscricao do CRC
	 */
	private String nroCrc;
	/**
	 * Unidade federativa do registro no CRC
	 */
	private String ufCrc;
	/**
	 * o Codigo de classificacao do CRC 
	 */
	private String codClassificacaoCRC;
	/**
	 * Tipo de Classificacao do CRC
	 */
	private String codTipoClassificacaoCRC;
	/**
	 * Indicador se o Contador é uma Pessoa Fisica (0) ou uma pessoa Juridica (1)
	 */
	private String tipoPessoa;
	/**
	 * Pessoa Fisica informado caso o Contador seja uma Pessoa fisica
	 */
	private PessoaFisica contador;
	/**
	 * Pessoa Juridica informada caso o contador seja a empresa de contabilidade
	 */
	private PessoaJuridica empresaContabilidade;
	/**
	 * A data de registro do CRC
	 */
	private Calendar dataRegistroCRC;
	/**
	 * A data de criacao desse registro, instanciado por padrao junto com a criacao do objeto
	 */
	private Calendar version;
	/**
	 * Cria o objeto Contador definindo o version para a hora de criacao do objeto
	 */	
	public Contador () {
		this.setVersion(Calendar.getInstance());
	}
	
	/**
	 * Passar um objeto {@link procempa.com.dto.PessoaJuridica} ou um objeto {@link procempa.com.dto.PessoaFisica} 
	 * @param pessoa a PessoaFisica ou Pessoajuridica que representa o contador
	 */
	public void definirContadorResponsavel(Pessoa pessoa){
		if (pessoa instanceof  PessoaJuridica ){
			this.empresaContabilidade = (PessoaJuridica) pessoa;
		}else{
			this.contador = (PessoaFisica) pessoa;
		}
	}
	
	/**
	 * @return the nroCrc
	 */
	public String getNroCrc() {
		return nroCrc;
	}
	/**
	 * @param nroCrc the nroCrc to set
	 */
	public void setNroCrc(String nroCrc) {
		this.nroCrc = nroCrc;
	}
	/**
	 * @return the ufCrc
	 */
	public String getUfCrc() {
		return ufCrc;
	}
	/**
	 * @param ufCrc the ufCrc to set
	 */
	public void setUfCrc(String ufCrc) {
		this.ufCrc = ufCrc;
	}
	/**
	 * @return the codClassificacaoCRC
	 */
	public String getCodClassificacaoCRC() {
		return codClassificacaoCRC;
	}
	/**
	 * @param codClassificacaoCRC the codClassificacaoCRC to set
	 */
	public void setCodClassificacaoCRC(String codClassificacaoCRC) {
		this.codClassificacaoCRC = codClassificacaoCRC;
	}
	/**
	 * @return the codTipoClassificacaoCRC
	 */
	public String getCodTipoClassificacaoCRC() {
		return codTipoClassificacaoCRC;
	}
	/**
	 * @param codTipoClassificacaoCRC the codTipoClassificacaoCRC to set
	 */
	public void setCodTipoClassificacaoCRC(String codTipoClassificacaoCRC) {
		this.codTipoClassificacaoCRC = codTipoClassificacaoCRC;
	}
	/**
	 * @return the tipoPessoa
	 */
	public String getTipoPessoa() {
		return tipoPessoa;
	}
	/**
	 * @param tipoPessoa the tipoPessoa to set
	 */
	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	/**
	 * @return the contador
	 */
	public PessoaFisica getContador() {
		return contador;
	}
	/**
	 * @param contador the contador to set
	 */
	public void setContador(PessoaFisica contador) {
		this.contador = contador;
	}
	/**
	 * @return the empresaContabilidade
	 */
	public PessoaJuridica getEmpresaContabilidade() {
		return empresaContabilidade;
	}
	/**
	 * @param empresaContabilidade the empresaContabilidade to set
	 */
	public void setEmpresaContabilidade(PessoaJuridica empresaContabilidade) {
		this.empresaContabilidade = empresaContabilidade;
	}
	/**
	 * @return the dataRegistroCRC
	 */
	public Calendar getDataRegistroCRC() {
		return dataRegistroCRC;
	}
	/**
	 * @param dataRegistroCRC the dataRegistroCRC to set
	 */
	public void setDataRegistroCRC(Calendar dataRegistroCRC) {
		this.dataRegistroCRC = dataRegistroCRC;
	}

	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}

}
