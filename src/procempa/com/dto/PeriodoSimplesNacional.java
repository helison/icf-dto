package procempa.com.dto;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Classe que contem os periodos do simples nacional que o estabelecimento possui
 * @author helison
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PeriodoSimplesNacional {
	
	/**
	 * Indica se o período do simples é de ME/EPP ou MEI
	 */
	private Integer regimeSimples;

	/**
	 * A data de inicio do perido de Simples Nacional
	 */
	private Calendar dataInicial;
	/**
	 * A data de fim do periodo de Simples Nacional
	 */
	private Calendar dataFinal;
	/**
	 * A data e hora de criacao do registro - definido por padrao na criacao do objeto
	 */
	private Calendar version;
	
	/**
	 * Cria o objeto PeriodoSimplesNacional definindo o version para a hora de criacao do objeto
	 */	
	public PeriodoSimplesNacional() {
		this.setVersion(Calendar.getInstance());
	}
	
	/**
	 * @return the dataInicial
	 */
	public Calendar getDataInicial() {
		return dataInicial;
	}
	/**
	 * @param dataInicial the dataInicial to set
	 */
	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}
	/**
	 * @return the dataFinal
	 */
	public Calendar getDataFinal() {
		return dataFinal;
	}
	/**
	 * @param dataFinal the dataFinal to set
	 */
	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}
	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}
	/**
	 * Data e hora da criacao do registro
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}
	/**
	 * retorna o tipo de regime do simples nacional
	 * @return 1 para ME/EPP e 2 para MEI
	 */
	public Integer getRegimeSimples() {
		return regimeSimples;
	}

	/**
	 * 1 para ME/EPP e 2 para MEI
	 * @param regimeSimples o tipo do regime do simples nacional
	 */
	public void setRegimeSimples(Integer regimeSimples) {
		this.regimeSimples = regimeSimples;
	}
}
