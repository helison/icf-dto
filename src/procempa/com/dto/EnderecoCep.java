package procempa.com.dto;

import java.util.List;

public class EnderecoCep {
	private int codLogradouro;
	private String nomeLogradouro;
	private String siglaTipoLogradouro;
	private String tipoLogradouro;
	private String nomeCidade;
	private int cep;
	private int cepNumeroInicial;
	private int cepNumeroFinal;
	private String ladoNumCep;
	private List<EnderecoBairro> bairrosLogradouro;

	public int getCodLogradouro() {
		return codLogradouro;
	}

	public void setCodLogradouro(int codLogradouro) {
		this.codLogradouro = codLogradouro;
	}

	public String getNomeLogradouro() {
		return nomeLogradouro;
	}

	public void setNomeLogradouro(String nomeLogradouro) {
		this.nomeLogradouro = nomeLogradouro;
	}

	public String getTipoLogradouro() {
		return tipoLogradouro;
	}

	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}


	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

	public int getCep() {
		return cep;
	}

	public void setCep(int cep) {
		this.cep = cep;
	}

	public int getCepNumeroInicial() {
		return cepNumeroInicial;
	}

	public void setCepNumeroInicial(int cepNumeroInicial) {
		this.cepNumeroInicial = cepNumeroInicial;
	}

	public int getCepNumeroFinal() {
		return cepNumeroFinal;
	}

	public void setCepNumeroFinal(int cepNumeroFinal) {
		this.cepNumeroFinal = cepNumeroFinal;
	}

	public String getLadoNumCep() {
		return ladoNumCep;
	}

	public void setLadoNumCep(String ladoNumCep) {
		this.ladoNumCep = ladoNumCep;
	}

	public List<EnderecoBairro> getBairrosLogradouro() {
		return bairrosLogradouro;
	}

	public void setBairrosLogradouro(List<EnderecoBairro> bairrosLogradouro) {
		this.bairrosLogradouro = bairrosLogradouro;
	}

	public String getSiglaTipoLogradouro() {
		return siglaTipoLogradouro;
	}

	public void setSiglaTipoLogradouro(String siglaTipoLogradouro) {
		this.siglaTipoLogradouro = siglaTipoLogradouro;
	}

}
