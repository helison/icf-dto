package procempa.com.dto;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * O socio da empresa com seus dados principais
 * 
 * @author helison
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Socio {
	public interface CodigoQualificacao {
		int ADMINISTRADOR = 3011;
		int NAO_INFORMADA = 3012;
		int DIRETOR = 3014;
		int EMPRESARIO = 3015;
		int SOCIO = 3016;
		int SOCIO_COMANDITARIO = 3018;
		int SOCIO_DE_INDUSTRIA = 3019;
		int PROCURADOR = 3020;
		int SOCIO_OSTENSIVO = 3021;
		int SINDICO = 3023;
		int SOCIO_GERENTE = 3024;
		int SOCIO_CAPITALISTA = 3025;
		int SOCIO_COMANDITADO = 3026;
		int SOCIEDADE_FILIADA = 3027;
		int EMPRESA_CONSORCIADA = 11992;
		int CONSELHO_DE_ADMINISTRACAO = 11994;
		int PAI_REPRESENTANTE = 11997;
		int MAE_REPRESENTANTE = 11998;
		int CURADOR_REPRESENTANTE = 11999;
		int TUTOR_REPRESENTANTE = 12000;
		int INVENTARIANTE = 12007;
		int LIQUIDANTE = 12008;
		int INTERVENTOR = 12009;
		int PRESIDENTE = 141355;
	}

	/**
	 * Caso o Socio seja uma pessoa Juridica, os dados dessa pessoa
	 */
	private PessoaJuridica socioPJ;
	/**
	 * Caso o Socio seja uma pessoa fisica, os dados dessa pessoa
	 */
	@Valid
	private PessoaFisica socioPF;
	/**
	 * O tipo de Socio (PF ou PJ)
	 */
	@NotEmpty(message = "O tipo de Socio devera ser informado")
	private String tipoPessoaSocia;
	/**
	 * A data de inicio do mandato do socio
	 */
	private Calendar dtaInicioMandato;

	/**
	 * A data de inclusao do socio
	 */
	@NotNull(message = "A data de inclusao do socio devera ser preenchida")
	private Calendar dtaInclusao;

	/**
	 * A data de fim do mantato do socio
	 */
	private Calendar dtaFimMandato;
	/**
	 * O codigo de qualificacao do socio
	 */
	@Min(value = 3011, message = "O Codigo de Qualificacao do Socio devera ser informado conforme interface de Codigo de Qualificacao")
	private Integer codQualificacao;
	/**
	 * A data de cadastro do Socio
	 */
	private Calendar dtaCadastro;
	/**
	 * O porcentual da participacao do Socio
	 */
	private BigDecimal prcParticipacao;

	/**
	 * O valor de participação do Socio
	 */
	private BigDecimal valorParticipacao;
	/**
	 * A indicacao se o Socio é o Responsavel pela empresa (S ou N)
	 */
	@Size(min = 1, max = 1, message = "Devera ser informado S ou N para socio principal")
	@NotNull(message = "Devera ser informado S ou N para socio principal")
	@NotBlank(message = "Devera ser informado S ou N para socio principal")
	private String indResponsavel;
	/**
	 * A data de criacao desse registro, definido por padrao com a data e hora atual
	 * na criacao do objeto
	 */
	private Calendar version;

	/**
	 * Cria o objeto Socio definindo o version para a hora de criacao do objeto
	 */
	public Socio() {
		this.setVersion(Calendar.getInstance());
	}

	/**
	 * @return the socioPJ
	 */
	public PessoaJuridica getSocioPJ() {
		return socioPJ;
	}

	/**
	 * Devera ser definido somente caso o Socio seja uma pessoa juridica
	 * 
	 * @param socioPJ the socioPJ to set
	 */
	public void setSocioPJ(PessoaJuridica socioPJ) {
		this.socioPJ = socioPJ;
	}

	/**
	 * @return the socioPF
	 */
	public PessoaFisica getSocioPF() {
		return socioPF;
	}

	/**
	 * Devera ser definido somente caso o Socio seja uma pessoa fisica
	 * 
	 * @param socioPF the socioPF to set
	 */
	public void setSocioPF(PessoaFisica socioPF) {
		this.socioPF = socioPF;
	}

	/**
	 * @return the tipoPessoaSocia
	 */
	public String getTipoPessoaSocia() {
		return tipoPessoaSocia;
	}

	/**
	 * @param tipoPessoaSocia the tipoPessoaSocia to set
	 */
	public void setTipoPessoaSocia(String tipoPessoaSocia) {
		this.tipoPessoaSocia = tipoPessoaSocia;
	}

	/**
	 * @return the dtaInicioMandato
	 */
	public Calendar getDtaInicioMandato() {
		return dtaInicioMandato;
	}

	/**
	 * @param dtaInicioMandato the dtaInicioMandato to set
	 */
	public void setDtaInicioMandato(Calendar dtaInicioMandato) {
		this.dtaInicioMandato = dtaInicioMandato;
	}

	/**
	 * @return the dtaFimMandato
	 */
	public Calendar getDtaFimMandato() {
		return dtaFimMandato;
	}

	/**
	 * @param dtaFimMandato the dtaFimMandato to set
	 */
	public void setDtaFimMandato(Calendar dtaFimMandato) {
		this.dtaFimMandato = dtaFimMandato;
	}

	/**
	 * @return the dtaCadastro
	 */
	public Calendar getDtaCadastro() {
		return dtaCadastro;
	}

	/**
	 * @param dtaCadastro the dtaCadastro to set
	 */
	public void setDtaCadastro(Calendar dtaCadastro) {
		this.dtaCadastro = dtaCadastro;
	}

	/**
	 * @return the prcParticipacao
	 */
	public BigDecimal getPrcParticipacao() {
		return prcParticipacao;
	}

	/**
	 * Define a porcentagem de participacao do socio no capital da empresa, caso nao
	 * exista definir como zero
	 * 
	 * @param prcParticipacao the prcParticipacao to set
	 */
	public void setPrcParticipacao(BigDecimal prcParticipacao) {
		this.prcParticipacao = prcParticipacao;
	}

	/**
	 * @return the valorParticipacao
	 */
	public BigDecimal getValorParticipacao() {
		return valorParticipacao;
	}

	/**
	 * @param valorParticipacao the valorParticipacao to set
	 */
	public void setValorParticipacao(BigDecimal valorParticipacao) {
		this.valorParticipacao = valorParticipacao;
	}

	/**
	 * @return the indResponsavel
	 */
	public String getIndResponsavel() {
		return indResponsavel;
	}

	/**
	 * Usado para identificar o socio principal
	 * 
	 * @param indResponsavel the indResponsavel to set
	 */
	public void setIndResponsavel(String indResponsavel) {
		this.indResponsavel = indResponsavel;
	}

	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}

	/**
	 * @return the codQualificacao
	 */
	public Integer getCodQualificacao() {
		return codQualificacao;
	}

	/**
	 * 
	 * *
	 * <table style="height: 450px;" border="1">
	 * <caption>Tipos de Socios da Empresa</caption> <tbody>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">C&oacute;d.SIAT</td>
	 * <td style="height: 18px; width: 191.312px;">Cargo-SIAT</td>
	 * <td style="height: 18px; width: 113.719px;">C&oacute;d.JUCISRS</td>
	 * <td style="height: 18px; width: 533.547px;">Qualifica&ccedil;&atilde;o
	 * JUCISRS (S&oacute;cio/Representante legal/Respons&aacute;vel CNPJ)</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3011</td>
	 * <td style="height: 18px; width: 191.312px;">Administrador</td>
	 * <td style="height: 18px; width: 113.719px;">5</td>
	 * <td style="height: 18px; width: 533.547px;">ADMINISTRADOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3012</td>
	 * <td style="height: 18px; width: 191.312px;">N&atilde;o informada</td>
	 * <td style="height: 18px; width: 113.719px;">&nbsp;</td>
	 * <td style="height: 18px; width: 533.547px;">&nbsp;</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3014</td>
	 * <td style="height: 18px; width: 191.312px;">Diretor</td>
	 * <td style="height: 18px; width: 113.719px;">10</td>
	 * <td style="height: 18px; width: 533.547px;">DIRETOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3015</td>
	 * <td style="height: 18px; width: 191.312px;">Empres&aacute;rio</td>
	 * <td style="height: 18px; width: 113.719px;">50</td>
	 * <td style="height: 18px; width: 533.547px;">EMPRES&Aacute;RIO</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3016</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio</td>
	 * <td style="height: 18px; width: 113.719px;">22</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3018</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio
	 * comandit&aacute;rio</td>
	 * <td style="height: 18px; width: 113.719px;">25</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO
	 * COMANDIT&Aacute;RIO</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3019</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio de
	 * ind&uacute;stria</td>
	 * <td style="height: 18px; width: 113.719px;">26</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO DE
	 * IND&Uacute;STRIA</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3020</td>
	 * <td style="height: 18px; width: 191.312px;">Procurador</td>
	 * <td style="height: 18px; width: 113.719px;">17</td>
	 * <td style="height: 18px; width: 533.547px;">PROCURADOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3021</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio ostensivo</td>
	 * <td style="height: 18px; width: 113.719px;">31</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO OSTENSIVO</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3023</td>
	 * <td style="height: 18px; width: 191.312px;">S&iacute;ndico</td>
	 * <td style="height: 18px; width: 113.719px;">19</td>
	 * <td style="height: 18px; width: 533.547px;">S&Iacute;NDICO
	 * (CONDOM&Iacute;NIO)</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3024</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio-gerente</td>
	 * <td style="height: 18px; width: 113.719px;">28</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO-GERENTE</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3025</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio Capitalista</td>
	 * <td style="height: 18px; width: 113.719px;">23</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO CAPITALISTA</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3026</td>
	 * <td style="height: 18px; width: 191.312px;">S&oacute;cio Comanditado</td>
	 * <td style="height: 18px; width: 113.719px;">24</td>
	 * <td style="height: 18px; width: 533.547px;">S&Oacute;CIO COMANDITADO</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">3027</td>
	 * <td style="height: 18px; width: 191.312px;">Sociedade filiada</td>
	 * <td style="height: 18px; width: 113.719px;">21</td>
	 * <td style="height: 18px; width: 533.547px;">SOCIEDADE FILIADA</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">11992</td>
	 * <td style="height: 18px; width: 191.312px;">Empresa consorciada</td>
	 * <td style="height: 18px; width: 113.719px;">20</td>
	 * <td style="height: 18px; width: 533.547px;">SOCIEDADE CONSORCIADA</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">11994</td>
	 * <td style="height: 18px; width: 191.312px;">Conselho de
	 * Administra&ccedil;&atilde;o</td>
	 * <td style="height: 18px; width: 113.719px;">8</td>
	 * <td style="height: 18px; width: 533.547px;">CONSELHEIRO DE
	 * ADMINISTRA&Ccedil;&Atilde;O</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">11997</td>
	 * <td style="height: 18px; width: 191.312px;">Pai/Representante</td>
	 * <td style="height: 18px; width: 113.719px;">14</td>
	 * <td style="height: 18px; width: 533.547px;">M&Atilde;E</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">11998</td>
	 * <td style="height: 18px; width: 191.312px;">M&atilde;e/Representante</td>
	 * <td style="height: 18px; width: 113.719px;">15</td>
	 * <td style="height: 18px; width: 533.547px;">PAI</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">11999</td>
	 * <td style="height: 18px; width: 191.312px;">Curador/Representante</td>
	 * <td style="height: 18px; width: 113.719px;">9</td>
	 * <td style="height: 18px; width: 533.547px;">CURADOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">12000</td>
	 * <td style="height: 18px; width: 191.312px;">Tutor/Representante</td>
	 * <td style="height: 18px; width: 113.719px;">35</td>
	 * <td style="height: 18px; width: 533.547px;">TUTOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">12007</td>
	 * <td style="height: 18px; width: 191.312px;">Inventariante</td>
	 * <td style="height: 18px; width: 113.719px;">12</td>
	 * <td style="height: 18px; width: 533.547px;">INVENTARIANTE</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">12008</td>
	 * <td style="height: 18px; width: 191.312px;">Liquidante</td>
	 * <td style="height: 18px; width: 113.719px;">13</td>
	 * <td style="height: 18px; width: 533.547px;">LIQUIDANTE</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">12009</td>
	 * <td style="height: 18px; width: 191.312px;">Interventor</td>
	 * <td style="height: 18px; width: 113.719px;">11</td>
	 * <td style="height: 18px; width: 533.547px;">INTERVENTOR</td>
	 * </tr>
	 * <tr style="height: 18px;">
	 * <td style="height: 18px; width: 90.4219px;">141355</td>
	 * <td style="height: 18px; width: 191.312px;">Presidente</td>
	 * <td style="height: 18px; width: 113.719px;">16</td>
	 * <td style="height: 18px; width: 533.547px;">PRESIDENTE</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 * <p>
	 * &nbsp;
	 * </p>
	 * 
	 * @param codQualificacao the codQualificacao to set
	 */

	public void setCodQualificacao(Integer codQualificacao) {
		this.codQualificacao = codQualificacao;
	}

	/**
	 * 
	 * @return the dtaInclusao
	 */
	public Calendar getDtaInclusao() {
		return dtaInclusao;
	}

	/**
	 * 
	 * @param dtaInclusao a data de inclusao do socio
	 */
	public void setDtaInclusao(Calendar dtaInclusao) {
		this.dtaInclusao = dtaInclusao;
	}

}
