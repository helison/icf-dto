package procempa.com.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe contendo os dados basicos da empresa conforme cadastro na Receita Federal Brasileira
 * @author helison
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DadosRFB {

	/**
	 * O nome fantasia da empresa
	 */
	private String nomeFantasiaPJ;
	/**
	 * A natureza juridica da empresa
	 */
	private String naturezaJuridica;
	/**
	 * A situacao do cadastro na Receita Federal
	 */
	private String situacaoCadastralRF;
	/**
	 * Caso a empresa esteja baixada, o motivo da Baixa na Receita Federal
	 */
	private String motivoBaixaRF;
	/**
	 * O porte da empresa
	 */
	private String porteEmpresa;
	/**
	 * O tipod e Unidade do Estabelecimento
	 */
	private String tipoUnidadeEstabelecRF;
	/**
	 * A forma de atuacao
	 */
	private String formaAtuacaoUnidRF;
	
	/**
	 * O código da natureza jurídica
	 */
	private String codNaturezaJuridica;
	
	
	public DadosRFB() {
	}
	
	/**
	 * @return the nomeFantasiaPJ
	 */
	public String getNomeFantasiaPJ() {
		return nomeFantasiaPJ;
	}
	/**
	 * @param nomeFantasiaPJ the nomeFantasiaPJ to set
	 */
	public void setNomeFantasiaPJ(String nomeFantasiaPJ) {
		this.nomeFantasiaPJ = nomeFantasiaPJ;
	}
	/**
	 * @return the naturezaJuridica
	 */
	public String getNaturezaJuridica() {
		return naturezaJuridica;
	}
	/**
	 * @param naturezaJuridica the naturezaJuridica to set
	 */
	public void setNaturezaJuridica(String naturezaJuridica) {
		this.naturezaJuridica = naturezaJuridica;
	}
	/**
	 * @return the situacaoCadastralRF
	 */
	public String getSituacaoCadastralRF() {
		return situacaoCadastralRF;
	}
	/**
	 * @param situacaoCadastralRF the situacaoCadastralRF to set
	 */
	public void setSituacaoCadastralRF(String situacaoCadastralRF) {
		this.situacaoCadastralRF = situacaoCadastralRF;
	}
	/**
	 * @return the motivoBaixaRF
	 */
	public String getMotivoBaixaRF() {
		return motivoBaixaRF;
	}
	/**
	 * @param motivoBaixaRF the motivoBaixaRF to set
	 */
	public void setMotivoBaixaRF(String motivoBaixaRF) {
		this.motivoBaixaRF = motivoBaixaRF;
	}
	/**
	 * @return the porteEmpresa
	 */
	public String getPorteEmpresa() {
		return porteEmpresa;
	}
	/**
	 * @param porteEmpresa the porteEmpresa to set
	 */
	public void setPorteEmpresa(String porteEmpresa) {
		this.porteEmpresa = porteEmpresa;
	}
	/**
	 * @return the tipoUnidadeEstabelecRF
	 */
	public String getTipoUnidadeEstabelecRF() {
		return tipoUnidadeEstabelecRF;
	}
	/**
	 * @param tipoUnidadeEstabelecRF the tipoUnidadeEstabelecRF to set
	 */
	public void setTipoUnidadeEstabelecRF(String tipoUnidadeEstabelecRF) {
		this.tipoUnidadeEstabelecRF = tipoUnidadeEstabelecRF;
	}
	/**
	 * @return the formaAtuacaoUnidRF
	 */
	public String getFormaAtuacaoUnidRF() {
		return formaAtuacaoUnidRF;
	}
	/**
	 * @param formaAtuacaoUnidRF the formaAtuacaoUnidRF to set
	 */
	public void setFormaAtuacaoUnidRF(String formaAtuacaoUnidRF) {
		this.formaAtuacaoUnidRF = formaAtuacaoUnidRF;
	}

	/**
	 * 
	 * @return o código da natureza jurídica
	 */
	public String getCodNaturezaJuridica() {
		return codNaturezaJuridica;
	}

	/**
	 * código da naturza jurídica no formato ###-#
	 * @param codNaturezaJuridica o código da natureza jurídica
	 */
	public void setCodNaturezaJuridica(String codNaturezaJuridica) {
		this.codNaturezaJuridica = codNaturezaJuridica;
	}

}
