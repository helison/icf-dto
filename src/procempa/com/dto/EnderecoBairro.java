package procempa.com.dto;

public class EnderecoBairro {

	private String nomeBairro;
	private int bairroNumeroInicial;
	private int bairroNumeroFinal;
	private String ladoNumBairro;

	public String getNomeBairro() {
		return nomeBairro;
	}

	public void setNomeBairro(String nomeBairro) {
		this.nomeBairro = nomeBairro;
	}

	public int getBairroNumeroInicial() {
		return bairroNumeroInicial;
	}

	public void setBairroNumeroInicial(int bairroNumeroInicial) {
		this.bairroNumeroInicial = bairroNumeroInicial;
	}

	public int getBairroNumeroFinal() {
		return bairroNumeroFinal;
	}

	public void setBairroNumeroFinal(int bairroNumeroFinal) {
		this.bairroNumeroFinal = bairroNumeroFinal;
	}

	public String getLadoNumBairro() {
		return ladoNumBairro;
	}

	public void setLadoNumBairro(String ladoNumBairro) {
		this.ladoNumBairro = ladoNumBairro;
	}

}
