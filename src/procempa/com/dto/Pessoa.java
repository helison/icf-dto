package procempa.com.dto;

import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe base para a criacao de pessoas, onde constam os dados basicos de uma pessoa fisica ou juridica
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pessoa {
	

	public interface ComplementoNome{
		int MASSA_FALIDA = 423;
		int EM_FALENCIA = 424;
		int EM_LIQUIDACAO = 425;
		int EM_LIQUIDACAO_EXTRA_JUDICIAL = 426;
		int EM_RECUPERACAO_JUDICIAL = 427;
		int INSOLVENCIA_CIVIL = 429;
		int ESPOLIO = 433;
		int OUTRA_SITUACAO_ESPECIAL_DA_PESSOA_FISICA = 430;
		int PESSOA_JURIDICA_EM_SUCESSAO_DE = 428;
	}
	/**
	 * CPF ou CNPJ da pessoa
	 */
	@NotEmpty
	@Size(min = 11, max = 14, message = "Devera ser informado um CPF com 11 posicoes ou um CNPJ com 14 posicoes")
	private String numDocumento;
	/**
	 * O nome da pessoa
	 */
	@NotEmpty(message = "O nome devera ser informado")
	private String desNome;
	/**
	 * O nome reduzido caso possua mais de 100 caracteres
	 */
	@Size(max = 60, message = "O nome reduzido deve possuir no maximo 60 caracteres")
	private String desNomeReduzido;
	/**
	 * A data de cadastro da pessoa
	 */
	@NotNull(message = "A data de cadastro devera ser informada")
	private Calendar dtaCadastro;
	/**
	 * Caso a pessoa esteja em situacao especial o codigo representando essa situacao
	 */
	private Integer codComplementoNome;
	/**
	 * A data e hora da criacao do registro - definido por padrao na instanciacao do objeto
	 */
	private Calendar version;
	/**
	 * Uma lista contendo todos os enderecos da pessoa
	 */
	@Valid
	@NotEmpty
	@Size(min = 1, message = "A pessoa deve possuir no minimo um endereco")
	private List<Endereco> enderecos;
	/**
	 * Uma lista contendo todos os contatos da pessoa
	 */
	@Valid
	private List<Contato> contatos;
	
	
/**
 * Cria o objeto Pessoa definindo o version para a hora de criacao do objeto 
 * @param numDocumento o CPF ou CNPJ da pessoa
 * @param desNome o nome da pessoa
 * @param desNomeReduzido o nome reduzido da pessoa caso ele tenha mais de 100 caracteres
 * @param dtaCadastro a data em que a pessoa foi cadastrada 
 * @param codComplementoNome o codigo de complemento do nome da pessoa em caso de situação especial
 * @param enderecos a lista contendo os enderecos da pessoa
 * @param contados a lista contendo os contatos da pessoa
 */
	public Pessoa(String numDocumento, String desNome, String desNomeReduzido, Calendar dtaCadastro,
			int codComplementoNome,  List<Endereco> enderecos, List<Contato> contados) {
		super();
		this.numDocumento = numDocumento;
		this.desNome = desNome;
		this.desNomeReduzido = desNomeReduzido;
		this.dtaCadastro = dtaCadastro;
		this.codComplementoNome = codComplementoNome;
		this.version = Calendar.getInstance();
		this.enderecos = enderecos;
		this.contatos = contados;
	}
	public Pessoa() {
	}
	
	/**
	 * @return the numDocumento
	 */
	
	public String getNumDocumento() {
		return numDocumento;
	}
	/**
	 * @param numDocumento the numDocumento to set
	 */
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	/**
	 * @return the desNome
	 */
	public String getDesNome() {
		return desNome;
	}
	/**
	 * nome da pessoa/empresa limitado a 100 caracteres.
	 * @param desNome the desNome to set
	 */
	public void setDesNome(String desNome) {
		this.desNome = desNome;
	}
	/**
	 * @return the desNomeReduzido
	 */
	public String getDesNomeReduzido() {
		return desNomeReduzido;
	}
	/**
	 * @param desNomeReduzido the desNomeReduzido to set
	 */
	public void setDesNomeReduzido(String desNomeReduzido) {
		this.desNomeReduzido = desNomeReduzido;
	}
	/**
	 * @return the dtaCadastro
	 */
	public Calendar getDtaCadastro() {
		return dtaCadastro;
	}
	/**
	 * @param dtaCadastro the dtaCadastro to set
	 */
	public void setDtaCadastro(Calendar dtaCadastro) {
		this.dtaCadastro = dtaCadastro;
	}
	/**
	 * @return the codComplementoNome
	 */
	public int getCodComplementoNome() {
		return codComplementoNome;
	}
	/**
	 * <table  border=1>
	 * <caption>Codigos de Complemento indicando situacao especial da pessoa</caption>
	 * <tbody>
	 * <tr>
	 * <td><strong>Codigo</strong></td>
	 * <td><strong>Situacao Especial</strong></td>
	 * </tr>
	 * <tr>
	 * <td>423</td>
	 * <td>Massa Falida</td>
	 * </tr>
	 * <tr>
	 * <td>424</td>
	 * <td>Em Fal&ecirc;ncia</td>
	 * </tr>
	 * <tr>
	 * <td>425</td>
	 * <td>Em Liquida&ccedil;&atilde;o</td>
	 * </tr>
	 * <tr>
	 * <td>426</td>
	 * <td>Em Liquida&ccedil;&atilde;o Extra-Judicial</td>
	 * </tr>
	 * <tr>
	 * <td>427</td>
	 * <td>Em Recupera&ccedil;&atilde;o Judicial</td>
	 * </tr>
	 * <tr>
	 * <td>429</td>
	 * <td>Insolv&ecirc;ncia Civil</td>
	 * </tr>
	 * <tr>
	 * <td>433</td>
	 * <td>Espolio</td>
	 * </tr>
	 * <tr>
	 * <td>430</td>
	 * <td>Outra situacao especial da pessoa fisica</td>
	 * </tr>
	 * <tr>
	 * <td>428</td>
	 * <td>Pessoa Juridica em Sucess&atilde;o de</td>
	 * </tr>
	 * </tbody>
	 * </table> 
	 * 
	 * @param codComplementoNome the codComplementoNome to set
	 */
	public void setCodComplementoNome(int codComplementoNome) {
		this.codComplementoNome = codComplementoNome;
	}
	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}
	/**
	 * @return the endereco
	 */
	public List<Endereco> getEnderecos() {
		return this.enderecos;
	}
	/**
	 * Define a lista de enderecos da pessoa.
	 * @param enderecos the enderecos to set
	 */
	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	/**
	 * @return the contado
	 */
	public List<Contato> getContatos() {
		return contatos;
	}
	/**
	 * 
	 * @param contatos the contatos to set
	 */
	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}
	
}
