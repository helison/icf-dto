package procempa.com.dto;

import java.util.Calendar;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Em caso de Empresa situada em Porto Alegre que tera sua inscricao criada e
 * obrigatoria a presenca de no minimo um endereco valido Caso seja uma empresa
 * matriz situada em Porto Alegre que presta serviÃ§o o endereco eh obrigatorio
 * Caso seja um socio, o endereco nao eh obrigatorio, porem eh importante Caso
 * seja um contador o endereco nao eh obrigatorio, porem eh importante
 * 
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Endereco {

	/**
	 * O codigo do logradouro segundo o CDL
	 */
	private Long codLogradouro;
	/**
	 * O nome do logradouro
	 */
	private String desLogradouro;
	/**
	 * O CEP do endereco
	 */
	@NotEmpty(message = "O CEP deve ser informado")
	private String numCep;
	/**
	 * O numero do endereco
	 */
	@NotEmpty(message = "O numero do endereco deve ser informado")
	private String numEndereco;
	/**
	 * Caso exista o numero da unidade
	 */
	private String numUnidade;
	/**
	 * O complemento do endereco
	 */
	private String complemento;
	/**
	 * O bairro do endereco
	 */
	private String bairro;
	/**
	 * A cidade do endereco
	 */
	private String cidade;
	/**
	 * O codigo da cidade
	 */
	private Long codigoCidade;
	/**
	 * A unidade federativa do endereco
	 */
	private String uf;
	/**
	 * O pais do endereco
	 */
	@NotEmpty(message = "O pais deve ser informado")
	private String pais;
	/**
	 * O codigo do pais
	 */
	private Long codigoPais;
	
	/**
	 * O tipo do logradouro
	 */
	private Long tipoLogradouro;
	/**
	 * Indica se o endereço é de correspondência
	 */
	private boolean indEnderecoCorrespondencia;	

	/**
	 * Cria o objeto Endereco definindo o version para a hora de criacao do objeto
	 */
	public Endereco() {
		this.setVersion(Calendar.getInstance());
	}

	/**
	 * @return the codigoCidade
	 */

	public Long getCodigoCidade() {
		return codigoCidade;
	}

	/**
	 * @param codigoCidade the codigoCidade to set
	 */
	public void setCodigoCidade(Long codigoCidade) {
		this.codigoCidade = codigoCidade;
	}

	/**
	 * @return the codigoPais
	 */
	public long getCodigoPais() {
		return codigoPais;
	}

	/**
	 * @param codigoPais the codigoPais to set
	 */
	public void setCodigoPais(Long codigoPais) {
		this.codigoPais = codigoPais;
	}

	/**
	 * A data e hora da criacao desse registro - campo criado com valor padrao na
	 * hora de criacao do objeto
	 */
	private Calendar version;

	/**
	 * @return the codLogradouro
	 */
	public Long getCodLogradouro() {
		return codLogradouro;
	}

	/**
	 * O codigo do logradouro seguindo o padrao do CDL
	 * 
	 * @param codLogradouro the codLogradouro to set
	 */
	public void setCodLogradouro(Long codLogradouro) {
		this.codLogradouro = codLogradouro;
	}

	/**
	 * @return the desLogradouro
	 */
	public String getDesLogradouro() {
		return desLogradouro;
	}

	/**
	 * @param desLogradouro the desLogradouro to set
	 */
	public void setDesLogradouro(String desLogradouro) {
		this.desLogradouro = desLogradouro;
	}

	/**
	 * @return the numCep
	 */
	public String getNumCep() {
		return numCep;
	}

	/**
	 * @param numCep the numCep to set
	 */
	public void setNumCep(String numCep) {
		this.numCep = numCep;
	}

	/**
	 * @return the numEndereco
	 */
	public String getNumEndereco() {
		return numEndereco;
	}

	/**
	 * @param numEndereco the numEndereco to set
	 */
	public void setNumEndereco(String numEndereco) {
		this.numEndereco = numEndereco;
	}

	/**
	 * @return the numUnidade
	 */
	public String getNumUnidade() {
		return numUnidade;
	}

	/**
	 * @param numUnidade the numUnidade to set
	 */
	public void setNumUnidade(String numUnidade) {
		this.numUnidade = numUnidade;
	}

	/**
	 * @return the complemento
	 */
	public String getComplemento() {
		return complemento;
	}

	/**
	 * @param complemento the complemento to set
	 */
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}

	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}

	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}

	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}

	public Long getTipoLogradouro() {
		return tipoLogradouro;
	}

	/**
	 * <table border=1>
	 * <caption>Códigos dos Tipos de Logradouros</caption>
	 * <tbody>
	 * <tr>
	 * <td>tipoLogradouro</td>
	 * <td>Nome</td>
	 * <td>Sigla</td>
	 * </tr>
	 * <tr>
	 * <td>78</td>
	 * <td>Acesso</td>
	 * <td>AC</td>
	 * </tr>
	 * <tr>
	 * <td>79</td>
	 * <td>Alameda</td>
	 * <td>AL</td>
	 * </tr>
	 * <tr>
	 * <td>81</td>
	 * <td>AS</td>
	 * <td>AS</td>
	 * </tr>
	 * <tr>
	 * <td>82</td>
	 * <td>Avenida</td>
	 * <td>AV</td>
	 * </tr>
	 * <tr>
	 * <td>83</td>
	 * <td>Beco</td>
	 * <td>BC</td>
	 * </tr>
	 * <tr>
	 * <td>84</td>
	 * <td>Belvedere</td>
	 * <td>BV</td>
	 * </tr>
	 * <tr>
	 * <td>85</td>
	 * <td>CA</td>
	 * <td>CA</td>
	 * </tr>
	 * <tr>
	 * <td>86</td>
	 * <td>Ciclovia</td>
	 * <td>CICL</td>
	 * </tr>
	 * <tr>
	 * <td>87</td>
	 * <td>CN</td>
	 * <td>CN</td>
	 * </tr>
	 * <tr>
	 * <td>88</td>
	 * <td>DIR</td>
	 * <td>DIR</td>
	 * </tr>
	 * <tr>
	 * <td>89</td>
	 * <td>Escadaria</td>
	 * <td>ESC</td>
	 * </tr>
	 * <tr>
	 * <td>90</td>
	 * <td>Esplanada</td>
	 * <td>ESP</td>
	 * </tr>
	 * <tr>
	 * <td>91</td>
	 * <td>Esquina</td>
	 * <td>ESQ</td>
	 * </tr>
	 * <tr>
	 * <td>92</td>
	 * <td>Estação</td>
	 * <td>EST</td>
	 * </tr>
	 * <tr>
	 * <td>93</td>
	 * <td>Estrada</td>
	 * <td>ESTR</td>
	 * </tr>
	 * <tr>
	 * <td>94</td>
	 * <td>Galeria</td>
	 * <td>GAL</td>
	 * </tr>
	 * <tr>
	 * <td>95</td>
	 * <td>Ilha</td>
	 * <td>I</td>
	 * </tr>
	 * <tr>
	 * <td>96</td>
	 * <td>Jardim</td>
	 * <td>JAR</td>
	 * </tr>
	 * <tr>
	 * <td>97</td>
	 * <td>Lago</td>
	 * <td>LAGO</td>
	 * </tr>
	 * <tr>
	 * <td>98</td>
	 * <td>LE</td>
	 * <td>LE</td>
	 * </tr>
	 * <tr>
	 * <td>99</td>
	 * <td>Largo</td>
	 * <td>LG</td>
	 * </tr>
	 * <tr>
	 * <td>100</td>
	 * <td>Mercado</td>
	 * <td>MER</td>
	 * </tr>
	 * <tr>
	 * <td>101</td>
	 * <td>Marina Municipal</td>
	 * <td>MRI</td>
	 * </tr>
	 * <tr>
	 * <td>102</td>
	 * <td>Praça</td>
	 * <td>PCA</td>
	 * </tr>
	 * <tr>
	 * <td>103</td>
	 * <td>Parque</td>
	 * <td>PRQ</td>
	 * </tr>
	 * <tr>
	 * <td>104</td>
	 * <td>PSG</td>
	 * <td>PSG</td>
	 * </tr>
	 * <tr>
	 * <td>105</td>
	 * <td>PSL</td>
	 * <td>PSL</td>
	 * </tr>
	 * <tr>
	 * <td>106</td>
	 * <td>PSN</td>
	 * <td>PSN</td>
	 * </tr>
	 * <tr>
	 * <td>1623</td>
	 * <td>PSO</td>
	 * <td>PSO</td>
	 * </tr>
	 * <tr>
	 * <td>4797</td>
	 * <td>Rua</td>
	 * <td>R</td>
	 * </tr>
	 * <tr>
	 * <td>4798</td>
	 * <td>RIO</td>
	 * <td>RIO</td>
	 * </tr>
	 * <tr>
	 * <td>4799</td>
	 * <td>RP</td>
	 * <td>RP</td>
	 * </tr>
	 * <tr>
	 * <td>4800</td>
	 * <td>RTL</td>
	 * <td>RTL</td>
	 * </tr>
	 * <tr>
	 * <td>4801</td>
	 * <td>SC</td>
	 * <td>SC</td>
	 * </tr>
	 * <tr>
	 * <td>4802</td>
	 * <td>TERM</td>
	 * <td>TERM</td>
	 * </tr>
	 * <tr>
	 * <td>4803</td>
	 * <td>Travessa</td>
	 * <td>TRAV</td>
	 * </tr>
	 * <tr>
	 * <td>4804</td>
	 * <td>Trevo</td>
	 * <td>TREV</td>
	 * </tr>
	 * <tr>
	 * <td>5720</td>
	 * <td>Travessia / Ponte</td>
	 * <td>TRVS</td>
	 * </tr>
	 * <tr>
	 * <td>5721</td>
	 * <td>Túnel</td>
	 * <td>TUN</td>
	 * </tr>
	 * <tr>
	 * <td>5722</td>
	 * <td>VA</td>
	 * <td>VA</td>
	 * </tr>
	 * <tr>
	 * <td>5723</td>
	 * <td>Viaduto</td>
	 * <td>VDT</td>
	 * </tr>
	 * <tr>
	 * <td>5724</td>
	 * <td>Viela</td>
	 * <td>VE</td>
	 * </tr>
	 * <tr>
	 * <td>5725</td>
	 * <td>Via</td>
	 * <td>VIA</td>
	 * </tr>
	 * <tr>
	 * <td>131355</td>
	 * <td>Recanto</td>
	 * <td>REC</td>
	 * </tr>
	 * <tr>
	 * <td>212355</td>
	 * <td>VTC</td>
	 * <td>VTC</td>
	 * </tr>
	 * </tbody>
	 * </table>
	 ** @param tipoLogradouro o tipo do logradouro a ser incluido
	 */
	public void setTipoLogradouro(Long tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	/**
	 * 
	 * @return true se a o endereco for de corresnpodencia
	 */
	public boolean isIndEnderecoCorrespondencia() {
		return indEnderecoCorrespondencia;
	}

	/**
	 * 
	 * @param indEnderecoCorrespondencia define se o endereco da empresa eh de correspondencia
	 */
	public void setIndEnderecoCorrespondencia(boolean indEnderecoCorrespondencia) {
		this.indEnderecoCorrespondencia = indEnderecoCorrespondencia;
	}
}
