package procempa.com.dto;

import java.util.Calendar;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * O contato da pessoa.
 * O contato pode ser um endereco de e-mail, um numero telefonico ou uma pagina web
 * O contato nao eh uma informacao obrigatorioa, porem eh importante
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contato {
	
	public interface TipoContato {
		int TELEFONE = 277;
		int EMAIL = 278;
		int PAGINA_WEB = 279;
	}
	public interface TipoTelefone {
		int RESIDENCIAL = 323;
		int COMERCIAL = 322;
	}
	/**
	 * O tipo de contato (telefone, e-mail, pagina web)
	 */
	@Min(value = 277 ,message = "O tipo de contato devera ser informado conforme interface TipoContato")
	@Max(value = 279, message = "O tipo de contato devera ser informado conforme interface TipoContato" )
	private Integer tipoContato;
	/**
	 * Caso seja um telefone, seu tipo (residencial, comercial)
	 */
	private Integer tipoTelefone;
	/**
	 * A informacao para o contato, numero de telefone, o endereco de e-mail ou a URL da pagina web
	 */
	@NotEmpty(message = "O contato deve ser informado")
	private String desContato;
	/**
	 * Uma indicacao se esse contato eh o contato principal desse tipo
	 */
	private String indPrincipal;
	/**
	 * A data e hora de criacao do registro - definido por padrao na criacao do objeto
	 */
	private Calendar version;
	/**
	 * Cria o objeto Contato definindo o version para a hora de criacao do objeto
	 */	
	public Contato() {
		this.setVersion(Calendar.getInstance());
	}
	/**
	 * @return the tipoContato
	 */
	public Integer getTipoContato() {
		return tipoContato;
	}
	/**
	 * Define o tipo do contato, aceita os seguitnes valores:
	 *  277 - Telefone
	 *  278 - Endereco de e-mail
	 *  279 - pagina web
	 * @param tipoContato the tipoContato to set
	 */
	public void setTipoContato(Integer tipoContato) {
		this.tipoContato = tipoContato;
	}
	/**
	 * @return the desContato
	 */
	public String getDesContato() {
		return desContato;
	}
	/**
	 * Campo onde eh informado o contato, que pode ser um numero de telefone, um endereco de e-mail ou uma pagina web.
	 * @param desContato the desContato to set
	 */
	public void setDesContato(String desContato) {
		this.desContato = desContato;
	}
	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}
	/**
	 * dia e hora em que as informacoes estao sendo processadas
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}
	/**
	 * @return the indPrincipal
	 */
	public String getIndPrincipal() {
		return indPrincipal;
	}
	/**
	 * Define se dos tipos de contato do mesmo grupo este eh o contato principal.
	 * Caso exista mais de um contato do mesmo tipo o primeiro devera ser definido como principal
	 * 
	 * @param indPrincipal the indPrincipal to set
	 */
	public void setIndPrincipal(String indPrincipal) {
		this.indPrincipal = indPrincipal;
	}
	/**
	 * @return the tipoTelefone
	 */
	public Integer getTipoTelefone() {
		return tipoTelefone;
	}
	/**
	 * Caso o contato seja do tipo Telefone informar o tipo de telefone, conforme abaixo
	 *323 para Residencial (pessoa fisica)
	 *322 para Comercial   (pessoa juridica)
	 * @param tipoTelefone the tipoTelefone to set
	 */
	public void setTipoTelefone(Integer tipoTelefone) {
		this.tipoTelefone = tipoTelefone;
	}
}
