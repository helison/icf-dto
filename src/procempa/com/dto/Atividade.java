package procempa.com.dto;

import java.util.Calendar;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



/**
 * <p>Atividade da empresa com suas principais caracteristicas
 * <p> Toda empresa deve possuir ao menos uma atividade principal, seguindo a regra para definicao da atividade principal
 * <p> Caso exista uma ou mais atividades de servico ela deve ser definida como atividade principal de servico, seguindo as regras especificas
 * @author helison.teixeira
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Atividade {
	/**
	 * A data inicial da atividade
	 */
	@NotNull(message = "A atividade deve possuir uma data de inicio")
	private Calendar dataInicial;
	/**
	 * A data final da atividade
	 */
	private Calendar dataFinal;
	/**
	 * O código CNAE da atividade com 9 digitos
	 */
	@NotEmpty
	@Size(min = 9, max = 9, message = "O codigo da atividade deve possuir 9 caracteres")
	private String codCnae;
	/**
	 * Indica se a atividade eh principal
	 */
	@NotEmpty(message = "Deve ser informado se a atividade principal (S / N)")
	private String flgPrincipal;
	/**
	 * Indica se a atividade eh a principal de servico 
	 */
	private String flgPrincipalServico;
	/**
	 * A data em que o cadastro da atividade foi efetuado
	 */
	private Calendar dtaCadastro;

	/**
	 * O horario em que o registro foi criado
	 */
	private Calendar version;
	
	/**
	 * Cria o objeto Atividade definindo o version para a hora de criacao do objeto
	 */	
	public Atividade() {
		this.setVersion(Calendar.getInstance());
	}
	
	/**
	 * @return the dataInicial
	 */
	public Calendar getDataInicial() {
		return dataInicial;
	}
	/**
	 * @param dataInicial the dataInicial to set
	 */
	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}
	/**
	 * @return the dataFinal
	 */
	public Calendar getDataFinal() {
		return dataFinal;
	}
	/**
	 * @param dataFinal the dataFinal to set
	 */
	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}
	/**
	 * @return the codCnae
	 */
	public String getCodCnae() {
		return codCnae;
	}
	/**
	 * Codigo CNAE seguindo o padrao da prefeitura com 9 digitos.
	 * @param codCnae the codCnae to set
	 */
	public void setCodCnae(String codCnae) {
		this.codCnae = codCnae;
	}
	/**
	 * @return the flgPrincipal
	 */
	public String getFlgPrincipal() {
		return flgPrincipal;
	}
	/**
	 * define se a atividade eh a atividade principal da empresa
	 * @param flgPrincipal the flgPrincipal to set
	 */
	public void setFlgPrincipal(String flgPrincipal) {
		this.flgPrincipal = flgPrincipal;
	}
	/**
	 * @return the flgPrincipalServico
	 */
	public String getFlgPrincipalServico() {
		return flgPrincipalServico;
	}
	/**
	 * define se a atividade eh a atividade principal de servico da empresa
	 * @param flgPrincipalServico the flgPrincipalServico to set
	 */
	public void setFlgPrincipalServico(String flgPrincipalServico) {
		this.flgPrincipalServico = flgPrincipalServico;
	}
	/**
	 * @return the dtaCadastro
	 */
	public Calendar getDtaCadastro() {
		return dtaCadastro;
	}
	/**
	 * @param dtaCadastro the dtaCadastro to set
	 */
	public void setDtaCadastro(Calendar dtaCadastro) {
		this.dtaCadastro = dtaCadastro;
	}
	/**
	 * @return the version
	 */
	public Calendar getVersion() {
		return version;
	}
	/**
	 * Define o horario que o registro foi criado, valor atribuido por padrao quando o objeto eh construido
	 * @param version the version to set
	 */
	public void setVersion(Calendar version) {
		this.version = version;
	}

}
