package procempa.com.dto;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Classe utilizada para transmitir as principais inconsistencias encontradas durante o processamento dos dados
 * @author helison
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inconsistencia {
	/**
	 * O CNPJ da empresa que ocorreu a inconsistencia
	 */
	private String cnpj;
	/**
	 * O tipo de operacao que estava sendo realizada conforme quadro de eventos
	 */
	private int tipoOperacao;
	/**
	 * o VO onde a inconsistencia foi encontrada
	 */
	private String origemInconsistencia;
	/**
	 * O campo onde a inconsistencia aconteceu
	 */
	private String campoInconsistencia;
	/**
	 * A descricao da inconsistencia
	 */
	private String desInconsistencia;
	/**
	 * A data de processamento
	 */
	private Calendar dataProcessamento;

	public Inconsistencia() {
	}
	/**
	 * @return the cnpj
	 */
	public String getCnpj() {
		return cnpj;
	}
	/**
	 * @param cnpj the cnpj to set
	 */
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	/**
	 * @return the tipoOperacao
	 */
	public int getTipoOperacao() {
		return tipoOperacao;
	}
	/**
	 * Seguir a tabela de Evento da tabela de apoio
	 * @param tipoOperacao the tipoOperacao to set
	 */
	public void setTipoOperacao(int tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	/**
	 * @return the origemInconsistencia
	 */
	public String getOrigemInconsistencia() {
		return origemInconsistencia;
	}
	/**
	 * A origem do dado da inconsistencia, como sÃ³cio, empresa, empresa matriz, contador, atividade, etc
	 * @param origemInconsistencia the origemInconsistencia to set
	 */
	public void setOrigemInconsistencia(String origemInconsistencia) {
		this.origemInconsistencia = origemInconsistencia;
	}
	/**
	 * @return the campoInconsistencia
	 */
	public String getCampoInconsistencia() {
		return campoInconsistencia;
	}
	/**
	 * O campo em que ocorreu a inconsistencia, como: numero do documento, nome da rua, CEP do endereco
	 * @param campoInconsistencia the campoInconsistencia to set
	 */
	public void setCampoInconsistencia(String campoInconsistencia) {
		this.campoInconsistencia = campoInconsistencia;
	}
	/**
	 * @return the desInconsistencia
	 */
	public String getDesInconsistencia() {
		return desInconsistencia;
	}
	/**
	 * breve descricao da inconsistencia, como: campo obrigatorio ausente, valor do campo invalido. 
	 * @param desInconsistencia the desInconsistencia to set
	 */
	public void setDesInconsistencia(String desInconsistencia) {
		this.desInconsistencia = desInconsistencia;
	}
	/**
	 * @return the dataProcessamento
	 */
	public Calendar getDataProcessamento() {
		return dataProcessamento;
	}
	/**
	 * data e horario em que a inconsistencia foi encontrada no processamento
	 * @param dataProcessamento the dataProcessamento to set
	 */
	public void setDataProcessamento(Calendar dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

}
